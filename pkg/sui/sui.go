// Package sui provides a frontend to the SUIT api.
package sui

import (
	"encoding/json"
	"net/http"
	"time"

	"gitlab.com/bigspeed/blazer/internal/pkg/cache"
)

const (
	ResultMiss = iota
	ResultId
	ResultUsername
)

// SuiUser is the response object from SUI.
type SuiUser struct {
	Error    bool   `json:"error"`
	Info     string `json:"info,omitempty"`
	Id       string `json:"id,omitempty"`
	Username string `json:"username,omitempty"`
}

func FindByIdOrUsername(user string) (result int, found string) {
	status, fetched := cache.FindInCache(user)
	if status != ResultMiss {
		// Found in cache!
		return status, fetched
	}

	// Not found in cache, search SUI
	client := &http.Client{Timeout: 5 * time.Second}

	// try to find by username first
	if user, err := client.Get("https://sui.sid72020123.repl.co/get_id/" + user); err == nil {
		// Found by username! Add to cache and return.
		defer user.Body.Close()

		var resp SuiUser
		json.NewDecoder(user.Body).Decode(&resp)

		cache.WriteToCache(resp.Username, resp.Id)
		return ResultId, resp.Id
	}

	// try to find by id
	if user, err := client.Get("https://sui.sid72020123.repl.co/get_username/" + user); err == nil {
		// Found by id! Add to cache and return.
		defer user.Body.Close()

		var resp SuiUser
		json.NewDecoder(user.Body).Decode(&resp)

		cache.WriteToCache(resp.Username, resp.Id)
		return ResultUsername, resp.Username
	}

	// Not found by username or id
	return ResultMiss, ""
}
