// Package cli provides the implementation for running in CLI mode.
package cli

import (
	"gitlab.com/bigspeed/blazer/internal/pkg/log"
	"gitlab.com/bigspeed/blazer/pkg/sui"
)

// Cli is the entry point for running in CLI mode.
func Cli(args []string) {
	for _, arg := range args {
		log.Logf(log.Load, "Searching for %s...", arg)
		result, found := sui.FindByIdOrUsername(arg)

		switch result {
		case sui.ResultId:
			log.Logf(log.Hit, "Found ID for %s: %s", arg, found)
		case sui.ResultUsername:
			log.Logf(log.Hit, "Found username for %s: %s", arg, found)
		case sui.ResultMiss:
			log.Logf(log.Miss, "No results found for %s", arg)
		}
	}
}
