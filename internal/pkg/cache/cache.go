// Package cache provides an interface to the suit-cache.json file.
package cache

import (
	"encoding/json"
	"os"

	"gitlab.com/bigspeed/blazer/internal/pkg/log"
)

type cacheType map[string]string

// FindInCache searches the cache for the passed user.
func FindInCache(user string) (result int, found string) {
	var cacheData cacheType

	loadFromCache(&cacheData)

	if val, ok := cacheData[user]; ok {
		return 1, val
	}

	// Not found by username, search for ID
	for key, val := range cacheData {
		if val == user {
			return 2, key
		}
	}

	return 0, ""
}

// WriteToCache writes the passed user to the cache.
func WriteToCache(user string, id string) {
	var cacheData cacheType
	loadFromCache(&cacheData)

	cacheData[user] = id

	writeCacheFile(cacheData)
}

// writeCacheFile creates a cache file from the passed cache struct.
func writeCacheFile(cache cacheType) {
	contents, _ := json.MarshalIndent(cache, "", "  ")
	_ = os.WriteFile("suit-cache.json", contents, 0644)
}

// loadFromCache loads the cache file into the passed cache struct.
func loadFromCache(cache *cacheType) {
	cacheFile, err := os.ReadFile("suit-cache.json")
	if err != nil {
		log.Logf(log.Info, "Cache file not found, creating new cache file.")
		cache = &cacheType{}
		writeCacheFile(*cache)
	}

	err = json.Unmarshal(cacheFile, &cache)
	if err != nil {
		log.Logf(log.Info, "Cache file is corrupt, resetting and ignoring cache.")
		cache = &cacheType{}
		writeCacheFile(*cache)
	}
}
