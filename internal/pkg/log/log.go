// Package log contains a shared log function for the application.
package log

import (
	"fmt"

	"github.com/fatih/color"
)

// Log types
const (
	Load = iota
	Hit
	Miss
	Err
	Info
)

// Logf prints a message to the console with the passed log type.
func Logf(logType int, message string, a ...any) {
	// hold the log prefix here!
	var prefix string

	switch logType {
	case Load:
		prefix = color.New(color.Bold, color.BgBlue).Sprint(" LOAD ")
	case Hit:
		prefix = color.New(color.Bold, color.BgGreen).Sprint(" HIT ")
	case Miss:
		prefix = color.New(color.Bold, color.BgBlack).Sprint(" MISS ")
	case Err:
		prefix = color.New(color.Bold, color.BgRed).Sprint(" ERR ")
	default:
		prefix = color.New(color.Bold, color.BgYellow).Sprint(" INFO ")
	}

	// Gets the format string for the message
	format := fmt.Sprintf("%17s %s\n", prefix, message)

	// Print the message to the console
	fmt.Printf(format, a...)
}
