# blazer

A conveniently familiar interface to SUI. Compatible with suit-cache.json.

If run with a username or ID as an argument (e.g. `blazer johndoe`), blazer will return the user's ID or username, respectively. If multiple arguments are given, blazer will loop through each one.

If run without any arguments (e.g. `blazer`), blazer will run in interactive mode.
