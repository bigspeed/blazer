package main

import (
	"os"

	"gitlab.com/bigspeed/blazer/internal/app/cli"
)

func main() {
	// Check if any arguments are passed
	if len(os.Args) > 1 {
		// Run in CLI mode
		cli.Cli(os.Args[1:])
	}
}
